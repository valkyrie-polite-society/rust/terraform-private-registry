# This is the build container.
FROM ekidd/rust-musl-builder:nightly-2020-08-26 AS build

# These layers will be rerun when either Cargo.toml or Cargo.lock change.

WORKDIR /home/rust/src
RUN USER=root cargo new --bin terraform-private-registry
WORKDIR /home/rust/src/terraform-private-registry
COPY Cargo.toml Cargo.lock ./
RUN cargo build --release

# These layers will be rerun when any of the source changes.

COPY src ./src
RUN cargo install  --path .

# This is the application container.
FROM alpine:edge

COPY --from=build /home/rust/src/terraform-private-registry/target/x86_64-unknown-linux-musl/release/terraform-private-registry /usr/bin
EXPOSE 8000
CMD ["/usr/bin/terraform-private-registry", "--providers"]
