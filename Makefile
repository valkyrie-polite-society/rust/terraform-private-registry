.PHONY: run runc image all

all: run

image:
	cargo clean
	docker build -t terraform-private-registry .
	docker tag terraform-private-registry gcr.io/adrienne-devops/terraform-registry
	docker push gcr.io/adrienne-devops/terraform-registry

run:
	cargo run -- --providers --sources-file examples/gitlab.yaml

runc:
	docker run --rm -it \
		-e GITLAB_TOKEN="${GITLAB_TOKEN}" \
		--publish 8000:8000 \
		--name terraform-private-registry \
		terraform-private-registry

clean:
	cargo clean
	rm -f *.log
