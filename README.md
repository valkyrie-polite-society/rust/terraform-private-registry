# Terraform Private Registry

An infrastructure shield sister's take on Terraform private registries

## How to run the thing

Install a Rust compiler and then

```
make
```

The command that will be run is

```
cargo run -- --providers --sources-file examples/gitlab.yaml
```

The contents of the configuration file are

```yaml
namespaces:
  valkyrie:
    path: valkyrie-polite-society/terraform-providers
    scm_type: gitlab
```

You can also provide sources with inline JSON:

```
cargo run -- --providers --sources-inline '{"namespaces":{"valkyrie":{"path":"valkyrie-polite-society/terraform-providers","scm_type":"gitlab"}}}'
```
