use anyhow::Result;
use consul::kv::*;
use reqwest::Client as HttpClient;
use reqwest::header;
use serde::Deserialize;
use std::collections::HashMap;
use std::fs;

#[derive(Clap, Clone, Debug)]
#[clap(name="terraform-registry", version = "1.0", author = "Adrienne Cohea <adriennecohea@gmail.com>")]
pub struct Configuration {
  #[clap(long, env = "REGISTRY_ENDPOINT", default_value = "example.com")] pub endpoint: String,
  #[clap(flatten)] pub features: Features,
  #[clap(long, env = "GITLAB_ENDPOINT", default_value = "gitlab.com")] pub gitlab_endpoint: String,
  #[clap(long, env = "GITLAB_TOKEN")] pub gitlab_token: String,
  #[clap(long, env = "SOURCES_FILE")] pub sources_file: Option<String>,
  #[clap(long, env = "SOURCES_INLINE")] pub sources_inline: Option<String>,
  #[clap(long, env = "SOURCES_CONSUL_URL")] pub sources_consul_url: Option<String>,
  #[clap(long, env = "SOURCES_CONSUL_TOKEN")] pub sources_consul_token: Option<String>,
  #[clap(long, env = "SOURCES_CONSUL_DATACENTER")] pub sources_consul_datacenter: Option<String>,
  #[clap(long, env = "SOURCES_CONSUL_KEY", default_value = "terraform-registry/sources.yaml")] pub sources_consul_key: String,
}

#[derive(Clap, Debug, Clone)]
pub struct Features {
  #[clap(long, about = "Enable the providers.v1 protocol")] pub providers: bool,
  #[clap(long, about = "Enable the modules.v1 protocol")] pub modules: bool,
  #[clap(long, about = "Enable the login.v1 protocol")] pub login: bool,
}

#[derive(Deserialize, Debug)]
pub struct Sources {
  pub namespaces: HashMap<String, ScmSource>
}

#[derive(Deserialize, Debug)]
pub struct ScmSource {
  pub scm_type: String,
  pub path: String
}

impl Configuration {
  pub fn load_sources(&self) -> Result<Sources> {
    if let Some(file) = self.sources_file.clone() {
      let contents = fs::read_to_string(file)?;

      return Ok(serde_yaml::from_str(contents.as_str())?);
    }

    if let Some(text) = self.sources_inline.clone() {
      if let Ok(sources) = serde_json::from_str::<Sources>(text.as_str()) {
        return Ok(sources);
      }

      if let Ok(sources) = serde_yaml::from_str::<Sources>(text.as_str()) {
        return Ok(sources);
      }

      return Err(anyhow::anyhow!("Unable to load sources from inline JSON or YAML."));
    }

    if let (Some(url), Some(token), Some(datacenter)) = (self.sources_consul_url.clone(), self.sources_consul_token.clone(), self.sources_consul_datacenter.clone()) {
      let key = self.sources_consul_key.clone();

      let client = get_consul_client(
        url.clone(),
        token,
        datacenter
      );

      if let Ok((Some(pair), _)) = client.get(key.as_str(), None) {
        if let Ok(utf8) = base64::decode_config(pair.Value.clone(), base64::STANDARD) {
          if let Ok(value) = std::str::from_utf8(&utf8) {
            if let Ok(sources) = serde_yaml::from_str::<Sources>(value) {
              return Ok(sources);
            }
          }
        }
      }

      let msg = format!("Unable to load sources from Consul {} with key {}", url, self.sources_consul_key);
      println!("{}", msg);
      return Err(anyhow::anyhow!(msg));
    }

    Err(anyhow::anyhow!("Unable to load sources. You must set --sources-file or --sources-inline, or the Consul sources options."))
  }
}

fn get_consul_client(url: String, token: String, datacenter: String) -> consul::Client{
  let mut headers = header::HeaderMap::new();
  headers.insert("X-Consul-Token", token.parse().unwrap());

  let client: HttpClient = reqwest::Client::builder()
    .default_headers(headers)
    .build().unwrap();

  let consul_config = consul::Config{
    address: url,
    datacenter: Some(datacenter),
    wait_time: None,
    http_client: client,
  };

  consul::Client::new(consul_config)
}
