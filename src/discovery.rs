use crate::configuration::Configuration;
use rocket::State;
use rocket_contrib::json::Json;
use std::collections::HashMap;

#[get("/.well-known/terraform.json")]
pub fn terraform_native_services(config: State<Configuration>) -> Json<HashMap<String, String>> {
  let mut services = HashMap::new();

  if config.features.providers {
    let (service, url) = endpoint(&config, "v1", "providers");
    services.insert(service, url);
  }

  if config.features.modules {
    let (service, url) = endpoint(&config, "v1", "modules");
    services.insert(service, url);
  }

  if config.features.login {
    let (service, url) = endpoint(&config, "v1", "login");
    services.insert(service, url);
  }

  Json(services)
}

fn endpoint(config: &Configuration, version: &str, service: &str) -> (String, String) {
  (format!("{}.{}", service, version), format!("{}/{}/{}/",
    config.endpoint,
    version,
    service
  ))
}
