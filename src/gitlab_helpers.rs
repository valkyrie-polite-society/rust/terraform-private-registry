use crate::configuration::Sources;
use crate::models::*;
use anyhow::Result;
use core::str::Split;
use gitlab::Gitlab;
use gitlab::api::{self, Query};
use gitlab::api::groups::Groups;
use gitlab::Group;
use gitlab::Project;
use gitlab::api::groups::subgroups::GroupSubgroups;
use gitlab::api::groups::projects::GroupProjects;
use gitlab::api::projects::releases::ProjectReleases;

pub fn search_for_group(group: &str, gitlab: &Gitlab) -> Result<Option<Group>> {
  let search = Groups::builder()
    .search(group)
    .build().unwrap();

  let results = api::paged(search, api::Pagination::All).query(gitlab)?;

  Ok(results.first().cloned())
}

pub fn list_subgroups(group: u64, gitlab: &Gitlab) -> Result<Vec<Group>> {
  let subgroups = GroupSubgroups::builder()
    .group(group)
    .build().unwrap();

  let results = api::paged(subgroups, api::Pagination::All).query(gitlab)?;

  Ok(results)
}

pub fn list_group_projects(group: u64, gitlab: &Gitlab) -> Result<Vec<Project>> {
  let group_projects = GroupProjects::builder()
    .group(group)
    .build().unwrap();

  let results =  api::paged(group_projects, api::Pagination::All).query(gitlab)?;

  Ok(results)
}

pub fn list_project_releases(project: u64, gitlab: &Gitlab) -> Result<Vec<Release>> {
  let project_releases = ProjectReleases::builder()
    .project(project)
    .build().unwrap();

  let results = api::paged(project_releases, api::Pagination::All).query(gitlab)?;

  Ok(results)
}

pub fn get_project(gitlab: &Gitlab, sources: &Sources, namespace: String, provider: String) -> Result<Option<Project>> {
  match get_group(gitlab, sources, namespace) {
    Ok(Some(group)) => {
      match list_group_projects(group.id.value(), gitlab) {
        Ok(projects) => Ok(projects.iter()
          .find(|project| project.name == format!("terraform-provider-{}", provider))
          .cloned()),
        Err(e) => Err(anyhow::anyhow!(e.to_string()))
      }
    },
    Ok(None) => Ok(None),
    Err(e) => Err(anyhow::anyhow!(e.to_string()))
  }
}

fn get_group(gitlab: &Gitlab, sources: &Sources, namespace: String) -> Result<Option<Group>> {
  match sources.namespaces.get(&namespace) {
    Some(source) => {
      let mut path = source.path.split("/");
      traverse(gitlab, &mut path, None)
    },
    None => Ok(None)
  }
}

fn traverse(gitlab: &Gitlab, segments: &mut Split<&str>, current_group: Option<Group>) -> Result<Option<Group>> {
  match segments.next() {
    None => Ok(current_group),
    Some(segment) => {
      match current_group {
        None => {
          match search_for_group(segment, gitlab)? {
            Some(base_group) => traverse(gitlab, segments, Some(base_group)),
            None => Ok(None)
          }
        },
        Some(base_group) => {
          let inner = list_subgroups(base_group.id.value(), gitlab)?
            .iter()
            .find(|g| g.path == segment)
            .cloned();

          match inner {
            Some(inner_group) => traverse(gitlab, segments, Some(inner_group)),
            None => Ok(None)
          }
        }
      }
    }
  }
}
