#![feature(proc_macro_hygiene, decl_macro)]

extern crate base64;
extern crate consul;
#[macro_use] extern crate rocket;
extern crate rocket_contrib;
#[macro_use] extern crate serde;
#[macro_use] extern crate clap;
extern crate gitlab;
extern crate regex;

use crate::clap::Clap;
use crate::configuration::{Configuration, Sources};
use gitlab::Gitlab;

mod configuration;
mod discovery;
mod gitlab_helpers;
mod login;
mod models;
mod modules;
mod providers;

fn main() {
  let config = Configuration::parse();
  let gitlab = Gitlab::new(config.gitlab_endpoint.clone(), config.gitlab_token.clone()).expect("Could not create GitLab client");
  let sources: Sources = config.load_sources().expect("Unable to retrieve sources");

  let mut server = rocket::ignite()
    .manage(config.clone())
    .manage(gitlab)
    .manage(sources)
    .mount("/", routes![
      discovery::terraform_native_services
    ]);

  if config.features.providers {
    server = server.mount("/v1/providers", routes![
      providers::list_available_versions,
      providers::find_provider_package,
     ]);
  }

  if config.features.modules {
    server = server.mount("/v1/modules", routes![
      modules::do_nothing
     ]);
  }

  if config.features.login {
    server = server.mount("/v1/login", routes![
      login::do_nothing
     ]);
  }

  server.launch();
}
