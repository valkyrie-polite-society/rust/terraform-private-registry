use regex::Regex;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ListAvailableVersionsResponse {
  pub versions: Vec<Version>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Version {
  pub version: String,
  pub protocols: Vec<String>,
  pub platforms: Vec<Platform>,
}

impl From<&Release> for Version {
  fn from(release: &Release) -> Self {
    Version {
      version: transform_tag_to_version(release),
      protocols: vec![format!("5.1")],
      platforms: get_supported_platforms(release)
    }
  }
}

pub fn transform_tag_to_version(release: &Release) -> String {
  match release.tag_name.strip_prefix("v") {
    Some(stripped) => stripped.to_owned(),
    None => release.tag_name.to_owned()
  }
}

fn get_supported_platforms(release: &Release) -> Vec<Platform> {

  let re = Regex::new(r"terraform-provider-(?P<provider>[a-zA-Z0-9]+)_(?P<version>[v\.0-9]+)_(?P<os>[a-zA-Z0-9]+)_(?P<arch>[a-zA-Z0-9]+).*").unwrap();

  release.assets.links.iter()
    .map(|link| link.name.as_str())
    .filter(|name| re.is_match(name))
    .map(|name| {
      let captures = re.captures(name).unwrap();

      Platform{
        os: captures.name("os").unwrap().as_str().to_string(),
        arch: captures.name("arch").unwrap().as_str().to_string(),
      }
    })
    .collect()
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Platform {
  pub os: String,
  pub arch: String,
}

pub fn get_provider_package(release: &Release, os: String, arch: String) -> Option<ProviderPackage> {
  let binary = release.assets.links.iter()
    .find(|link| link.name.ends_with(format!("{}_{}.zip", os, arch).as_str()))
    .cloned()?;

  let shasums_link = release.assets.links.iter()
    .find(|link| link.name.ends_with("SHA256SUMS"))
    .cloned();

  let shasums_signature_link = release.assets.links.iter()
    .find(|link| link.name.ends_with("SHA256SUMS.sig"))
    .cloned();

  if let (Some(shasums), Some(shasums_signature)) = (shasums_link, shasums_signature_link) {
    return Some(ProviderPackage{
      protocols: vec!["5.1".to_string()],
      os,
      arch,
      filename: binary.name,
      download_url: binary.url,
      shasums_url: shasums.url,
      shasums_signature_url: shasums_signature.url,
      shasum: format!(""),
      signing_keys: SigningKeys{
        gpg_public_keys: vec![
          GpgPublicKey{
            key_id: format!(""),
            ascii_armor: format!(""),
            trust_signature: format!(""),
            source: format!(""),
            source_url: format!(""),
          }
        ]
      }
    });
  }

  return Some(ProviderPackage{
    protocols: vec!["5.1".to_string()],
    os,
    arch,
    filename: binary.name,
    download_url: binary.url,
    shasums_url: format!(""),
    shasums_signature_url: format!(""),
    shasum: format!(""),
    signing_keys: SigningKeys{
      gpg_public_keys: vec![
        GpgPublicKey{
          key_id: format!(""),
          ascii_armor: format!(""),
          trust_signature: format!(""),
          source: format!(""),
          source_url: format!(""),
        }
      ]
    }
  });
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ProviderPackage {
  pub protocols: Vec<String>,
  pub os: String,
  pub arch: String,
  pub filename: String,
  #[serde(rename = "download_url")]
  pub download_url: String,
  #[serde(rename = "shasums_url")]
  pub shasums_url: String,
  #[serde(rename = "shasums_signature_url")]
  pub shasums_signature_url: String,
  pub shasum: String,
  #[serde(rename = "signing_keys")]
  pub signing_keys: SigningKeys,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SigningKeys {
  #[serde(rename = "gpg_public_keys")]
  pub gpg_public_keys: Vec<GpgPublicKey>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GpgPublicKey {
  #[serde(rename = "key_id")]
  pub key_id: String,
  #[serde(rename = "ascii_armor")]
  pub ascii_armor: String,
  #[serde(rename = "trust_signature")]
  pub trust_signature: String,
  pub source: String,
  #[serde(rename = "source_url")]
  pub source_url: String,
}

/// Release information.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Release {
  /// The tag name of the release
  pub tag_name: String,
  /// The release description
  pub description: String,
  /// The name of the release
  pub name: String,
  /// Link to description
  pub description_html: String,
  /// Creation date
  pub created_at: String,
  /// Release date
  pub released_at: String,
  /// Path to the commit for this release
  pub commit_path: String,
  /// Path to the tag for this release
  pub tag_path: String,
  pub assets: ReleaseAssets,
}

/// Collection of release assets
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ReleaseAssets {
  pub sources: Vec<AssetSource>,
  pub links: Vec<AssetLink>,
}

/// Source information for a release asset
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct AssetSource {
  pub format: String,
  pub url: String,
}

/// Link for a release asset
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct AssetLink {
  pub id: u64,
  pub name: String,
  pub url: String,
  pub external: bool,
  pub link_type: String,
}
