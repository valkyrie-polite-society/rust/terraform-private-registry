use crate::configuration::Sources;
use crate::models::*;
use anyhow::Result;
use rocket::State;
use rocket_contrib::json::Json;
use gitlab::Gitlab;
use itertools::Itertools;

use crate::gitlab_helpers::*;

//
// List Available Versions
//
// GET /<namespace>/<provider>/versions
//
// This operation determines which versions are currently available for a particular provider.
//
// https://www.terraform.io/docs/internals/provider-registry-protocol.html#list-available-versions
//
// Sample request:
//
// curl 'https://registry.terraform.io/v1/providers/hashicorp/random/versions'
//

#[get("/<namespace>/<provider>/versions")]
pub fn list_available_versions(gitlab: State<Gitlab>, sources: State<Sources>, namespace: String, provider: String) -> Result<Option<Json<ListAvailableVersionsResponse>>> {
  match get_project(gitlab.inner(), &sources, namespace, provider) {
    Ok(Some(project)) => {
      let releases = list_project_releases(project.id.value(), gitlab.inner())?;
      let versions = releases.iter()
        .map_into::<Version>()
        .collect_vec();

      Ok(Some(Json(ListAvailableVersionsResponse{
        versions
      })))
    },
    Ok(None) => Ok(None),
    Err(e) => Err(anyhow::anyhow!(e.to_string()))
  }
}

// Find a Provider Package
//
// GET /<namespace>/<provider>/<version>/download/<os>/<arch>
//
// This operation returns the download URL of and associated metadata about the distribution package
// for a particular version of a provider for a particular operating system and architecture.
// Terraform CLI uses this operation after it has selected the newest available version matching the
// configured version constraints, in order to find the zip archive containing the plugin itself.

// https://www.terraform.io/docs/internals/provider-registry-protocol.html#find-a-provider-package
//
// Sample request:
//
// curl 'https://registry.terraform.io/v1/providers/hashicorp/random/2.0.0/download/linux/amd64'
#[get("/<namespace>/<provider>/<version>/download/<os>/<arch>")]
pub fn find_provider_package(gitlab: State<Gitlab>, sources: State<Sources>, namespace: String, provider: String, version: String, os: String, arch: String) -> Result<Option<Json<ProviderPackage>>> {
  match get_project(gitlab.inner(), &sources, namespace, provider) {
    Ok(Some(project)) => {

      let releases = list_project_releases(project.id.value(), gitlab.inner())?;
      let selected = releases.iter()
        .find(|release| {transform_tag_to_version(release) == version});

      match selected {
        Some(rel) => {
          match crate::models::get_provider_package(rel, os, arch) {
            Some(pkg) => Ok(Some(Json(pkg))),
            None => Ok(None)
          }
        },
        None => Ok(None),
      }
    },
    Ok(None) => Ok(None),
    Err(e) => Err(anyhow::anyhow!(e.to_string()))
  }
}
